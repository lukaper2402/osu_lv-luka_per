#Skripta zadatak_1.py uˇcitava podatkovni skup iz data_C02_emission.csv .
#Dodajte programski kod u skriptu pomo ́cu kojeg možete odgovoriti na sljede ́ca pitanja:
#a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka veliˇcina? Postoje li izostale ili
#duplicirane vrijednosti? Obrišite ih ako postoje. Kategoriˇcke veliˇcine konvertirajte u tip
#category.
#b) Koja tri automobila ima najve ́cu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
#ime proizvo  ̄daˇca, model vozila i kolika je gradska potrošnja.
#c) Koliko vozila ima veliˇcinu motora izme  ̄du 2.5 i 3.5 L? Kolika je prosjeˇcna C02 emisija
#plinova za ova vozila?
#d) Koliko mjerenja se odnosi na vozila proizvo  ̄daˇca Audi? Kolika je prosjeˇcna emisija C02
#plinova automobila proizvo  ̄daˇca Audi koji imaju 4 cilindara?
#e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjeˇcna emisija C02 plinova s obzirom na
#broj cilindara?
#f) Kolika je prosjeˇcna gradska potrošnja u sluˇcaju vozila koja koriste dizel, a kolika za vozila
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
#g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najve ́cu gradsku potrošnju goriva?
#h) Koliko ima vozila ima ruˇcni tip mjenjaˇca (bez obzira na broj brzina)?
#i) Izraˇcunajte korelaciju izme  ̄du numeriˇckih veliˇcina. Komentirajte dobiveni rezultat.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
 
data =  pd.read_csv('data_C02_emission.csv')
 
#a)
print(f"Broj mjerenja: {len(data)}")

# Kojeg je tipa svaka veličina
print(data.dtypes)
# Postoje li duplicirane vrijednosti
data=data.dropna()#uklanjanje nan
data=data.drop_duplicates()

print(f"Postoje duplicirane vrijednosti: {data}")

# Kategoricke velicine pretvorite u tip category
columns_for_convert = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type', ]
for column in columns_for_convert:
    data[column] = data[column].astype('category')


#b)
fuel_consumption_city = data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].sort_values(by='Fuel Consumption City (L/100km)')
print(f'Najmanja gradska potrosnja: {fuel_consumption_city.head(3)}')
print(f'Najveca gradska potrosnja: {fuel_consumption_city.tail(3)}')

#c)
data_engine=data[(data["Engine Size (L)"]>2.5)& (data['Engine Size (L)']<3.5)]

print(f"Ima {len(data_engine)} vozila")

print(data_engine["CO2 Emissions (g/km)"].mean())

 #d)
audi=data[(data["Make"]=="Audi")]
print(f"Ima {len(audi)} audi vozila")

audi_4=audi[(audi["Cylinders"])==4]
print(audi_4["CO2 Emissions (g/km)"].mean())

#e)
grouped_cylinders=data.groupby("Cylinders")

for number, group in grouped_cylinders:
    print(f"Prosječna emisija CO2 plinova za vozila sa {number} cilindara: {group['CO2 Emissions (g/km)'].mean()}")

#f)

disel=data[(data["Fuel Type"]=="D")]
gasoline=data[(data["Fuel Type"]=="X")]

print(disel["Fuel Consumption City (L/100km)"].mean())

print(gasoline["Fuel Consumption City (L/100km)"].mean())

print(disel["Fuel Consumption City (L/100km)"].median())

print(gasoline["Fuel Consumption City (L/100km)"].median())

print(f"Prosječna gradska potrošnja za vozila koja koriste dizel: {disel['Fuel Consumption City (L/100km)'].mean().__round__(2)}")
print(f"Prosječna gradska potrošnja za vozila koja koriste benzin: {gasoline['Fuel Consumption City (L/100km)'].mean().__round__(2)}")
print(f"Medijalna gradska potrošnja za vozila koja koriste dizel: {disel['Fuel Consumption City (L/100km)'].median().__round__(2)}")
print(f"Medijalna gradska potrošnja za vozila koja koriste benzin: {gasoline['Fuel Consumption City (L/100km)'].median().__round__(2)}")
#g)

print(disel[disel["Cylinders"]==4].sort_values(by="Fuel Consumption City (L/100km)").tail(1))

#h)

manual_transmission = data[data['Transmission'].str[0] == 'M']
print(f"Broj vozila sa rucnim tipom mjenjaca: {len(manual_transmission)}")

#i)
print ( data.corr(numeric_only=True))


print(data.iloc[0:2, 1:2])




