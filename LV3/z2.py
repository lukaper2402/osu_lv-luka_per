#Napišite programski kod koji  ́ce prikazati sljede ́ce vizualizacije:
#a) Pomo ́cu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.
#b) Pomo ́cu dijagrama raspršenja prikažite odnos izme  ̄du gradske potrošnje goriva i emisije
#C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izme  ̄du
#veliˇcina, obojite toˇckice na dijagramu raspršenja s obzirom na tip goriva.
#c) Pomo ́cu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip
#goriva. Primje ́cujete li grubu mjernu pogrešku u podacima?
#d) Pomo ́cu stupˇcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu
#groupby.
#e) Pomo ́cu stupˇcastog grafa prikažite na istoj slici prosjeˇcnu C02 emisiju vozila s obzirom na
#broj cilindara.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
 
data =  pd.read_csv('data_C02_emission.csv')
 
columns_for_convert = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type', ]
for column in columns_for_convert:
    data[column] = data[column].astype('category')
 
# a)
plt.figure()
plt.xlabel('CO2 Emissions')
plt.hist(data['CO2 Emissions (g/km)'], bins=30)
plt.show()

#b)
plt.figure()
plt.scatter(data['Fuel Consumption City (L/100km)'], data['CO2 Emissions (g/km)'], c=data['Fuel Type'].cat.codes)
plt.legend
plt.show()
 
# c)
data.boxplot(column='Fuel Consumption Hwy (L/100km)', by='Fuel Type')
plt.show()
 
# d)
data_grouped = data.groupby('Fuel Type').size()
data_grouped.plot(kind='bar')
plt.show()
 
# e)
avg_co2_emission_per_cylinder = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
avg_co2_emission_per_cylinder.plot(kind='bar')
plt.show()
