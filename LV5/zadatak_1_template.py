#Skripta zadatak_1.py generira umjetni binarni klasifikacijski problem s dvije
#ulazne veliˇcine. Podaci su podijeljeni na skup za uˇcenje i skup za testiranje modela.
#a) Prikažite podatke za uˇcenje u x 1 − x 2 ravnini matplotlib biblioteke pri ˇcemu podatke obojite
#s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
#marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
#cmap kojima je mogu ́ce definirati boju svake klase.
#b) Izgradite model logistiˇcke regresije pomo ́cu scikit-learn biblioteke na temelju skupa poda-
#taka za uˇcenje.
#c) Prona  ̄dite u atributima izgra  ̄denog modela parametre modela. Prikažite granicu odluke
#nauˇcenog modela u ravnini x 1 − x2 zajedno s podacima za uˇcenje. Napomena: granica
#odluke u ravnini x1 − x2 definirana je kao krivulja: θ0 + θ1 x 1 + θ2 x 2 = 0.
#d) Provedite klasifikaciju skupa podataka za testiranje pomo ́cu izgra  ̄denog modela logistiˇcke
#regresije. Izraˇcunajte i prikažite matricu zabune na testnim podacima. Izraˇcunate toˇcnost,
#preciznost i odziv na skupu podataka za testiranje.
#e) Prikažite skup za testiranje u ravnini x1 − x 2 . Zelenom bojom oznaˇcite dobro klasificirane
#primjere dok pogrešno klasificirane primjere oznaˇcite crnom bojom.

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
 
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
 
 
X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)
 
# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)
 
 
# a)
plt.figure()
plt.scatter(X_train[:, 0], X_train[:, 1], marker='o', cmap = 'viridis', c=y_train)
plt.scatter(X_test[:, 0], X_test[:, 1], marker='x', cmap='plasma', c=y_test)
 
# b)
LogRegression = LogisticRegression()
LogRegression.fit(X_train, y_train)
 
# c)
parameters = LogRegression.coef_
intercept = LogRegression.intercept_
 
theta1 = parameters[0][0]
theta2 = parameters[0][1]
theta0 = intercept[0]
 
print(parameters, intercept)
print(theta1, theta2, theta0)
 
a = -theta2 / theta1
b = -theta0 / theta1
 
x = X_train[:, 1]*a+b
plt.plot(X_train[:,1], x, 'r--')
plt.show()
#x = np.linspace(np.min(X_train), np.max(X_train), 100)
#y = a * x + b
#plt.plot(x, y, 'k-')
#upper = np.max(X_train[:, 1])
#lower = np.min(X_train[:, 1])
#plt.fill_between(x, y, upper, where=(y<=upper), interpolate=True, color='blue', alpha=0.2)
#plt.fill_between(x, y, lower, where=(y>=lower), interpolate=True, color='yellow', alpha=0.2)
#plt.show()
 
# d)
y_test_p = LogRegression.predict(X_test)
 
confusionMatrix = confusion_matrix(y_test, y_test_p)
disp = ConfusionMatrixDisplay(confusionMatrix)
 
disp.plot()
plt.show()
 
print(classification_report(y_test, y_test_p))
 
 
# e)
trues = []
falses = []
 
for i in range(len(y_test)):
    if y_test[i] == y_test_p[i]:
        trues.append([X_test[i, 0], X_test[i, 1]])
    else:
        falses.append([X_test[i, 0], X_test[i, 1]])
 
trues = np.array(trues)
falses = np.array(falses)
 
plt.figure()
plt.scatter(x = trues[:, 0], y = trues[:, 1], marker = 'o', color = 'green')
plt.scatter(x = falses[:, 0], y = falses[:, 1], marker = 'x', color = 'red')
plt.show()