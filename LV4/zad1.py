#Skripta zadatak_1.py uˇcitava podatkovni skup iz data_C02_emission.csv .
#Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju os-
#talih numeriˇckih ulaznih veliˇcina. Detalje oko ovog podatkovnog skupa mogu se prona ́ci u 3.
#laboratorijskoj vježbi.
#a) Odaberite željene numeriˇcke veliˇcine specificiranjem liste s nazivima stupaca. Podijelite
#podatke na skup za uˇcenje i skup za testiranje u omjeru 80%-20%.
#b) Pomo ́cu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova
#o jednoj numeriˇckoj veliˇcini. Pri tome podatke koji pripadaju skupu za uˇcenje oznaˇcite
#plavom bojom, a podatke koji pripadaju skupu za testiranje oznaˇcite crvenom bojom.
#c) Izvršite standardizaciju ulaznih veliˇcina skupa za uˇcenje. Prikažite histogram vrijednosti
#jedne ulazne veliˇcine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja
#transformirajte ulazne veliˇcine skupa podataka za testiranje.
#d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
#povežite ih s izrazom 4.6.
#e) Izvršite procjenu izlazne veliˇcine na temelju ulaznih veliˇcina skupa za testiranje. Prikažite
#pomo ́cu dijagrama raspršenja odnos izme  ̄du stvarnih vrijednosti izlazne veliˇcine i procjene
#dobivene modelom.
#f) Izvršite vrednovanje modela na naˇcin da izraˇcunate vrijednosti regresijskih metrika na
#skupu podataka za testiranje.
#g) Što se doga  ̄da s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj
#ulaznih veliˇcina?

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error

data=pd.read_csv('data_C02_emission.csv')
x=data[['Engine Size (L)','Cylinders', 'Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]
y=data['CO2 Emissions (g/km)']

#a)
x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.2, random_state=1)

#B
plt.figure("B")
plt.scatter(y_train, x_train['Fuel Consumption City (L/100km)'], c='b')
plt.scatter(y_test, x_test['Fuel Consumption City (L/100km)'], c='r')

plt.show()

#C
ss=StandardScaler()
x_train_nn=ss.fit_transform(x_train)
x_test_nn=ss.transform(x_test)

x_train_n=pd.DataFrame(x_train_nn, columns=x_train.columns)
x_test_n=pd.DataFrame(x_test_nn, columns=x_test.columns)

plt.figure()
x_train_n['Engine Size (L)'].plot(kind='hist')
x_train['Engine Size (L)'].plot(kind='hist')

plt.show()

#D
linearModel=lm.LinearRegression()
linearModel.fit(x_train_n, y_train)

print(linearModel.coef_)
print(linearModel.intercept_)

#E
y_test_p=linearModel.predict(x_test_n)

plt.figure()
plt.scatter(y_test, y_test_p)
plt.show()

#F
mae=mean_absolute_error(y_test, y_test_p)
mape=mean_absolute_percentage_error(y_test, y_test_p)
mse=mean_squared_error(y_test, y_test_p)

print(f"MAE {mae}")
print(f"MAPE {mape}")
print(f"MSE {mse}")

#g
print("----------Original Test----------")
linearModel.fit(ss.transform(x_test_n), y_test)
print(linearModel.coef_)
print(linearModel.intercept_)

print("----------1/2 Test----------")
linearModel.fit(ss.transform(x_test_n[:int((len(x_test_n)-1)/2)]), y_test[:int((len(y_test)-1)/2)])
print(linearModel.coef_)
print(linearModel.intercept_)

print("----------1/4 Test----------")
linearModel.fit(ss.transform(x_test_n[:int((len(x_test_n)-1)/4)]), y_test[:int((len(y_test)-1)/4)])
print(linearModel.coef_)
print(linearModel.intercept_)