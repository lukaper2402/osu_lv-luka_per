#import bibilioteka
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.discriminant_analysis import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from tensorflow import keras
from keras.models import Sequential
from keras import layers
from keras.models import load_model

#učitavanje dataseta
iris = datasets.load_iris()
data = pd.DataFrame(data = iris.data, columns=iris.feature_names)
print(data.head())
data['target'] = iris.target # 0 Setosa, # 1 Versicolor, # 2 Virginica
##################################################
#1. zadatak
##################################################
# a)#Prikažite odnos duljine latice i ˇcašice svih pripadnika klase Virginica pomo´cu scatter
#dijagrama zelenom bojom. Dodajte na isti dijagram odnos duljine latice i ˇcašice svih
#pripadnika klase Setosa, sivom bojom. Dodajte naziv dijagrama i nazive osi te legendu.
#Komentirajte prikazani dijagram.
virginica = data[data['target'] == 2]
versicolor = data[data['target'] == 1]
setosa = data[data['target'] == 0]
plt.figure()
plt.scatter(virginica['sepal length (cm)'], virginica['petal length (cm)'], color="green", label="Virginica")
plt.scatter(setosa['sepal length (cm)'], setosa['petal length (cm)'], color="gray", label="Satosa")
plt.xlabel("Sepal length")
plt.ylabel("Petal length")
plt.title('Sepal vs petal length')
plt.legend()
plt.show()
#b)#Pomo´cu stupˇcastog dijagrama prikažite najve´cu vrijednost širine ˇcašice za sve tri klase
#cvijeta. Dodajte naziv dijagrama i nazive osi. Komentirajte prikazani dijagram.
barplot = {'Virginica': virginica['sepal width (cm)'].max(), 'Versicolor': versicolor['sepal width (cm)'].max(), 'Setosa': setosa['sepal width (cm)'].max()}
plt.figure()
plt.bar(list(barplot.keys()), list(barplot.values()))
plt.xlabel("Classes")
plt.ylabel("Max sepal width")
plt.title("Max sepal width for every class")
plt.show()
#c)#Koliko jedinki pripadnika klase Setosa ima ve´cu širinu ˇcašice od prosjeˇcne širine ˇcašice te
#klase?
counter = 0
setosa_mean_sepal_width = setosa['sepal width (cm)'].mean()
for x in setosa['sepal width (cm)']:
    if x > setosa_mean_sepal_width:
        counter = counter + 1
print(counter, "ima vecu siricu casice od prosjeka")
#################################################
# 2. zadatak
#a) Prona ¯ dite optimalni broj klastera K za klasifikaciju cvijeta irisa algoritmom K srednjih
#vrijednosti.
#b) Grafiˇcki prikažite lakat metodu.
#c) Primijenite algoritam K srednjih vrijednosti koji ´ce prona´ci grupe u podatcima. Koristite
#vrijednot K dobivenu u prethodnom zadatku.
#d) Dijagramom raspršenja prikažite dobivene klastere. Obojite ih razliˇcitim bojama (zelena,
#žuta i naranˇcasta). Centroide obojite crvenom bojom. Dodajte nazive osi, naziv dijagrama i
#legendu. Komentirajte prikazani dijagram.
#e) Usporedite dobivene klase sa njihovim stvarnim vrijednostima. Izraˇcunajte toˇcnost klasifikacije

#################################################
iris = datasets.load_iris()
data = pd.DataFrame(data = iris.data, columns=iris.feature_names)
data['target'] = iris.target # 0 Setosa, # 1 Versicolor, # 2 Virginica
x = data[['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']]
y = data['target']
#a)
values = []
K_values = range(1, 11)
for i in K_values:
    x = x.copy()
    km = KMeans(n_clusters=i)
    km.fit(x)
    values.append(km.inertia_)

#b)
plt.figure()
plt.plot(K_values, values)
plt.title("Elbow metoda")
plt.xlabel("K")
plt.ylabel("Vrijednost")
plt.tight_layout()
plt.show()

#c)
best_k = 3
km = KMeans(n_clusters=best_k)
km.fit(x)
predicted = km.predict(x)


#d)
cmap = []
for label in predicted:
    if label==0:
        cmap.append("green")
    elif label == 1:
        cmap.append("yellow")
    elif label == 2:
        cmap.append("orange")
plt.figure()
plt.scatter(x['sepal length (cm)'],x['sepal width (cm)'], c=cmap)
plt.scatter(km.cluster_centers_[:,0],km.cluster_centers_[:,1], marker='x', c='red', s=100)
green_patch = patches.Patch(color="green", label="Setosa")
yellow_patch = patches.Patch(color="yellow", label="Versicolor")
orange_patch = patches.Patch(color="orange", label="Virginica")
plt.legend(handles=[green_patch, yellow_patch, orange_patch])
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')
plt.title('Podatkovni primjeri')
plt.show()

#e)
counter = 0

for i in range(0, len(predicted)):
    if predicted[i] == y[i]:
        counter = counter + 1
print(counter/len(x) * 100)

##################################################
#3. zadatak
##################################################
iris = datasets.load_iris()
data = pd.DataFrame(data = iris.data, columns=iris.feature_names)
data['target'] = iris.target # 0 Setosa, # 1 Versicolor, # 2 Virginica

x = data[['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']]
y = data['target']

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, stratify=y)

y_test_conf = y_test
scaler = StandardScaler()
X_train_n = scaler.fit_transform(X_train)
X_test_n = scaler.transform(X_test)
y_train = keras.utils.to_categorical(y_train, 3)
y_test = keras.utils.to_categorical(y_test, 3)
#a)
model = Sequential()
model.add(layers.Input(shape=(4,)))
model.add(layers.Dense(12, activation="relu"))
model.add(layers.Dropout(0.2))
model.add(layers.Dense(7, activation="relu"))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(5, activation="relu"))
model.add(layers.Dense(3, activation="softmax"))

model.summary()

#b)
model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])
#c)
model.fit(X_train_n, y_train, epochs = 450, batch_size = 7)

# d)
# model.save('model.keras')
# e)
model = load_model('model.keras')
model.summary()

evaluate = model.evaluate(X_test_n, y_test)
print("Accuarcy", evaluate[1])
print("Loss", evaluate[0])

# f)
predict = model.predict(X_test_n)
predict = np.argmax(predict, axis=1)
conf_mat = confusion_matrix(y_test_conf, predict)
print(conf_mat)