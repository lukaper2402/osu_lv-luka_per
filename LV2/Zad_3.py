#Skripta zadatak_3.py uˇcitava sliku ’road.jpg ’. Manipulacijom odgovaraju ́ce
#numpy matrice pokušajte:
#a) posvijetliti sliku,
#b) prikazati samo drugu ˇcetvrtinu slike po širini,
#c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu,
#d) zrcaliti sliku

import numpy as np
import matplotlib.pyplot as plt

img = plt.imread ("road.jpg")
img=img[:,:,0]

plt.figure()
plt.imshow(img,cmap ="gray")
plt.show () 

#a)
img_b=img.copy()
img_b+=70
img_b[img_b<70]=255
plt.imshow(img_b,cmap="gray")
plt.show()

#b)
img2=img[:,160:320]
plt.imshow(img2,cmap="gray")
plt.show()

#c)
img_rot=np.rot90(img)
img_rot=np.rot90(img_rot)
img_rot=np.rot90(img_rot)

plt.imshow(img_rot,cmap="gray")
plt.show()

#d)
img_mirror=np.fliplr(img)
plt.imshow(img_mirror,cmap="gray")
plt.show()
