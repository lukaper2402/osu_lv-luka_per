#Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
#ženama. Skripta zadatak_2.py uˇcitava dane podatke u obliku numpy polja data pri ˇcemu je u
#prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a tre ́ci
#stupac polja je masa u kg.
#a) Na temelju veliˇcine numpy polja data, na koliko osoba su izvršena mjerenja?
#b) Prikažite odnos visine i mase osobe pomo ́cu naredbe matplotlib.pyplot.scatter.
#c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
#d) Izraˇcunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom
#podatkovnom skupu.
#e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
#muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.

import numpy as np
import matplotlib.pyplot as plt

Data=np.loadtxt("data.csv",  delimiter=",", skiprows=1)
#a)
print(f"Broj ljudi: {Data.shape[0]}")

h=Data[:,1]
w=Data[:,2]
h50=Data[::50,1]
w50=Data[::50,2]
print(len(h50))
#b)
plt.scatter(h,w)
plt.show()
#c)
plt.scatter(h50,w50)
plt.show()

#d)
print(f"Najveca visina: {np.max(h)}")
print(f"Najmanja visina: {np.min(h)}")
print(f"Srednja visina: {np.mean(h)}")

#e)
man=Data[(Data[:,0]==1)]
man_h=man[:,1]
print(f"Najveca visina muskarca: {np.max(man_h)}")
print(f"Najmanja visina muskarca: {np.min(man_h)}")
print(f"Srednja visina muskaraca: {np.mean(man_h)}")

women=Data[(Data[:,0]==0)]
women_h=women[:,1]
print(f"Najveca visina muskarca: {np.max(women_h)}")
print(f"Najmanja visina muskarca: {np.min(women_h)}")
print(f"Srednja visina muskaraca: {np.mean(women_h)}")