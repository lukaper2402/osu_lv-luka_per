#Napišite Python skriptu koja  ́ce uˇcitati tekstualnu datoteku naziva SMSSpamCollection.txt
#[1]. Ova datoteka sadrži 5574 SMS poruka pri ˇcemu su neke oznaˇcene kao spam, a neke kao ham.
#Primjer dijela datoteke:
#ham Yup next sto
#a) Izraˇcunajte koliki je prosjeˇcan broj rijeˇci u SMS porukama koje su tipa ham, a koliko je
#prosjeˇcan broj rijeˇci u porukama koje su tipa spam.
#b) Koliko SMS poruka koje su tipa spam završava uskliˇcnikom ?

hams = []
spams = []
ham_counter = 0
spam_counter = 0
counter = 0

file = open("SMSSpamCollection.txt")
all_sms = file.read().strip().split('\n')
file.close()

for sms in all_sms:
    if sms.startswith('spam'):
        spams.append(sms)
    elif sms.startswith('ham'):
        hams.append(sms)

# a
for ham in hams:
    ham_counter += len(ham.split())

for spam in spams:
    spam_counter += len(spam.split())

print(f"Average number of words in ham: {ham_counter/len(hams)}")
print(f"Average number of words in spam: {spam_counter/len(spams)}")

# b
for spam in spams:
    if spam.endswith("!"):
        counter+=1

print(f"Number of spams that end with ! is: {counter}")