#Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
#nekakvu ocjenu i nalazi se izme  ̄du 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju
#sljede ́cih uvjeta:

try:
    x=float(input("Unesi broj od 0.0 do 1.0: "))
except:
        print("Nisi unio broj")
        exit()
if x<0.0 or x>1.0:
    print("x nije u intervalu")
    exit()       


elif(x>=0.9):
    print("A")
elif(x>=0.8):
    print("B")
elif(x>=0.7):
    print("C")
elif(x>=0.6):
    print("D")
else:
    print("F")

