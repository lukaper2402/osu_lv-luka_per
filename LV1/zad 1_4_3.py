#Napišite program koji od korisnika zahtijeva unos brojeva u beskonaˇcnoj petlji
#sve dok korisnik ne upiše „ Done “ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
#potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
#(npr. slovo umjesto brojke) na naˇcin da program zanemari taj unos i ispiše odgovaraju ́cu poruku.

list=[]
while True:
    x=input("Unesi broj: ")
    if(x=="Done"):
        break
    try:
        x=int(x)
    except:
        print("Unesi broj!!")
        continue


    list.append(x)
print("Unešeno je: ", len(list), "brojeva")

print("Minimalna vrijednost: ", min(list))
print("Maksimalna vrijednost: ", max(list))

print("Srednja: ",sum(list)/len(list) )
list.sort()
print(list)