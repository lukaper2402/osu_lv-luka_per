#Napišite Python skriptu koja  ́ce uˇcitati tekstualnu datoteku naziva song.txt .
#Potrebno je napraviti rjeˇcnik koji kao kljuˇceve koristi sve razliˇcite rijeˇci koje se pojavljuju u
#datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijeˇc (kljuˇc) pojavljuje u datoteci.
#Koliko je rijeˇci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

words = {}
all_words = []
one_time_counter = 0

file = open("song.txt")
for line in file:
    line = line.rstrip()
    all_words += line.split()
file.close()

for word in all_words:
    if word not in words:
        words[word]=1
    else:
        words[word]+=1

for word in words:
    if words[word] == 1:
        one_time_counter +=1
        print(word)

print(one_time_counter)