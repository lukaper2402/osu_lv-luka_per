#Napišite skriptu koja  ́ce uˇcitati izgra  ̄denu mrežu iz zadatka 1. Nadalje, skripta
#treba uˇcitati sliku test.png sa diska. Dodajte u skriptu kod koji  ́ce prilagoditi sliku za mrežu,
#klasificirati sliku pomo ́cu izgra  ̄dene mreže te ispisati rezultat u terminal. Promijenite sliku
#pomo ́cu nekog grafiˇckog alata (npr. pomo ́cu Windows Paint-a nacrtajte broj 2) i ponovo pokrenite
#skriptu. Komentirajte dobivene rezultate za razliˇcite napisane znamenke.

import numpy as np
from tensorflow import keras
from keras import layers, utils
from keras.models import load_model
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix

image=utils.load_img('slika.png',color_mode="grayscale",target_size=(28,28))

image=utils.img_to_array(image)
image_s=image.astype("float32") / 255
image_s=np.expand_dims(image_s,-1)
image_s=image_s.reshape(1,784)

model=load_model('model2/')

model.summary()

y_predict=model.predict(image_s)

print(y_predict.argmax())