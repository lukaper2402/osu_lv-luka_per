#import bibilioteka
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score 
from sklearn.neighbors import KNeighborsClassifier
from matplotlib.colors import ListedColormap
from tensorflow import keras
from keras import layers
#from keras.models import load_model
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV

##################################################
#1. zadatak
##################################################

#učitavanje dataseta
data = pd.read_csv("titanic.csv")

#a)
female_pass = data[data['Sex']=='female']
print("Broj zena za koje postoje podatci:", len(female_pass))

#b)
died = data[data['Survived']==0]
ratio = len(died)/len(data)
print("Postotak ljudi koji nije prezivio je", ratio*100, "%")

#c)
total_men = len(data[data['Sex']=='male'])
total_female = len(data[data['Sex']=='female'])
survived = data[data['Survived']==1]
total_men_survived = len(survived[survived['Sex']=='male'])
total_female_survived = len(survived[survived['Sex']=='female'])
ratio_men = (total_men_survived/total_men)*100
ratio_female = (total_female_survived/total_female)*100
sex_data = {'Male' : ratio_men, 'Female': ratio_female}
sex = list(sex_data.keys())
values = list(sex_data.values()) 
color=['green', 'yellow']
plt.bar(sex, values, color=color, width=0.4)
plt.xlabel("Spol")
plt.ylabel("Postotak")
plt.title("Preživjeli muškarci i žene Titanika")
plt.show()

#corr_men, _ = pearsonr(data[data['Sex']=='male'], ratio_men)
#corr_fem, _ = pearsonr(data[data['Sex']=='female'], ratio_female)
#print("Korelacija muskog spola i postotka prezivljavanja: ", corr_men)
#print("Korelacija zenskog spola i postotka prezivljavanja: ", corr_fem) 

#d)
data_male = data[data['Sex']=='male']
data_male = data_male[data_male['Survived']==1]
data_female = data[data['Sex']=='female']
data_female = data_female[data_female['Survived']==1]
avg_female_survived_age = data_female['Age'].mean()
avg_male_survived_age = data_male['Age'].mean()
print("Prosjecna dob prezivjelih zena je: ", avg_female_survived_age)
print("Prosjecna dob prezivjelih muskaraca je: ", avg_male_survived_age)

#e)
first_class_data = data[data['Pclass']==1]
second_class_data = data[data['Pclass']==2]
third_class_data = data[data['Pclass']==3]
first_class_data = first_class_data[first_class_data['Sex']=='male']
second_class_data = second_class_data[second_class_data['Sex']=='male']
third_class_data = third_class_data[third_class_data['Sex']=='male']
first_class_data = first_class_data[first_class_data['Survived']==1]
second_class_data = second_class_data[second_class_data['Survived']==1]
third_class_data = third_class_data[third_class_data['Survived']==1]

print("Najstariji prezivjeli muskarac u prvoj klasi ima ", first_class_data['Age'].max(), " godina")
print("Najstariji prezivjeli muskarac u drugoj klasi ima ", second_class_data['Age'].max(), " godina")
print("Najstariji prezivjeli muskarac u trecoj klasi ima ", third_class_data['Age'].max(), " godina")  

##################################################
#2. zadatak
##################################################

""" def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()

    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
    np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl) 

#učitavanje dataseta
                    
data = pd.read_csv("titanic.csv")

data = data.dropna()
data = data.drop_duplicates()

data['Sex'].replace('female', 0, inplace=True)
data['Sex'].replace('male', 1, inplace=True)

data['Embarked'].replace('S', 0, inplace=True)
data['Embarked'].replace('C', 5, inplace=True)
data['Embarked'].replace('Q', 10, inplace=True)


X = data[['Pclass', 'Sex', 'Fare', 'Embarked']]
y = data['Survived']

#train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1)

scaler = StandardScaler()
X_train_n = scaler.fit_transform(X_train)
X_test_n = scaler.transform(X_test)

#a)
model = KNeighborsClassifier(n_neighbors = 5)
model.fit(X_train_n, y_train)
y_train_pred = model.predict(X_train_n)
y_test_pred = model.predict(X_test_n)

# Dobivam grešku u liniji 98 koja kaže da X ima 2 varijable, a očekuje da ima 4. Kada sam gledao po breakpoint-ovima X do te linije ima 4 varijable, ali nisam uspio naći rješenje
# plot_decision_regions(X_train_n, y_train, classifier=model)
# plt.xlabel('x_1')
# plt.ylabel('x_2')
# plt.legend(loc='upper left')
# plt.title("KNN (K={}) Accuracy: {:0.3f}".format(5, accuracy_score(y_train, y_train_pred)))
# plt.tight_layout()
# plt.show()  

#b)
y_train_p_KNN = model.predict(X_train_n)
y_test_p_KNN = model.predict(X_test_n)

print("KNN (K=5) Accuracy, Training Set: {:0.3f}".format(accuracy_score(y_train, y_train_pred)))
print("KNN (K=5) Accuracy, Testing Set: {:0.3f}".format(accuracy_score(y_test, y_test_pred)))

#c)

pipe = Pipeline([('scaler', StandardScaler()), ('knn', KNeighborsClassifier())])
param_grid = {'knn__n_neighbors': np.arange(1, 50)}
grid = GridSearchCV(pipe, param_grid, cv=10)
grid.fit(X_train, y_train)
print("Najbolji parametri: ", grid.best_params_)

#d)
print("Tocnost: ", grid.best_score_)
print("Tocnost na testnom skupu: ", grid.score(X_test, y_test))  """

##################################################
#3. zadatak
##################################################

#učitavanje podataka:
""" data = pd.read_csv("titanic.csv")

data = data.dropna()
data = data.drop_duplicates()

data['Sex'].replace('female', 0, inplace=True)
data['Sex'].replace('male', 1, inplace=True)

data['Embarked'].replace('S', 0, inplace=True)
data['Embarked'].replace('C', 1, inplace=True)
data['Embarked'].replace('Q', 2, inplace=True)


X = data[['Pclass', 'Sex', 'Fare', 'Embarked']]
y = data['Survived']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1)

#a)
model = keras.Sequential()

model.add(layers.Input(shape=(4,)))
model.add(layers.Dense(12, activation='relu'))
model.add(layers.Dense(8, activation='relu'))
model.add(layers.Dense(4, activation='relu'))
model.add(layers.Dense(1, activation="sigmoid"))

print(model.summary())

#b)
model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])
#c)
model.fit(X_train, y_train, epochs=100, batch_size=5)
#d)
model.save('FK_ispit_model.h5')
#e)

loaded_model = load_model("FK_ispit_model.h5")
score = loaded_model.evaluate(X_test, y_test)

#f)
predictions = loaded_model.predict(X_test)
predictions[predictions > 0.5] = 1
predictions[predictions <= 0.5] = 0

conf_matrix = confusion_matrix(y_test, predictions)
print("Matrica zabune:")
print(conf_matrix) 
 """