import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.discriminant_analysis import StandardScaler
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix, precision_score, recall_score, accuracy_score
from sklearn.model_selection import train_test_split
import sklearn.linear_model as lm


diabetes = pd.read_csv('diabetes.csv')

# 2.0 X and y split
x = diabetes[['Pregnancies', 'Glucose', 'BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction', 'Age']]
y = diabetes['Outcome']

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=32)

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# 2.1 Logistic regression
model = lm.LogisticRegression()
model.fit(X_train, y_train)

# 2.1 Set classification
y_pred = model.predict(X_test)

# 2.2 Plot confusion matrix
confusionMatrix = confusion_matrix(y_test, y_pred)
disp = ConfusionMatrixDisplay(confusionMatrix)
disp.plot()
plt.show()

# 2.3 Accuracy, Recall and Precision

accuracy = accuracy_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)

print('Accuracy: ', accuracy, ' Recall: ', recall, ' Precision: ', precision)