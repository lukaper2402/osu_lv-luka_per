import numpy as np
import pandas as pd
from tensorflow import keras
from keras.api import layers
from sklearn.model_selection import train_test_split
import sklearn.linear_model as lm

diabetes = pd.read_csv('diabetes.csv')

# 3.1 Model neural network
x = diabetes[['Pregnancies', 'Glucose', 'BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction', 'Age']]
y = diabetes['Outcome']

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=32)

model = keras.Sequential()
model.add(layers.Input(shape=(8,)))
model.add(layers.Dense(12, activation='relu'))
model.add(layers.Dense(8, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))

model.summary()

# 3.2 Compile
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# 3.3 Fit
model.fit(X_train, y_train, epochs=10, batch_size=10, verbose=2)

# 3.4 Save on disk
model.save('FCN/diabetes_model.h5')
del model