import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix
from tensorflow import keras
from keras.api import layers
from keras.api.models import load_model
from sklearn.model_selection import train_test_split
import sklearn.linear_model as lm

diabetes = pd.read_csv('diabetes.csv')

# 3.1 Model neural network
x = diabetes[['Pregnancies', 'Glucose', 'BloodPressure', 'SkinThickness', 'Insulin', 'BMI', 'DiabetesPedigreeFunction', 'Age']]
y = diabetes['Outcome']

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=32)

model = load_model('FCN/diabetes_model.h5')

model.summary()

# 3.5 Evaluate
evalutation = model.evaluate(X_test, y_test, verbose=2)

# 3.6 Predict
predictions = model.predict(X_test, verbose=2)
predictions[predictions > 0.5] = 1
predictions[predictions <= 0.5] = 0

confusionMatrix = confusion_matrix(y_test, predictions)
for pred, actual in zip(predictions, y_test):
    print(f"Prediction: {pred}, Actual: {actual}")
disp = ConfusionMatrixDisplay(confusionMatrix)
disp.plot()
plt.show()