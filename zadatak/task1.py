import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

diabetes = pd.read_csv('diabetes.csv')

# 1.1 Number of data
print('Number of data: ', len(diabetes))

# 1.2 Drop duplicates and none
diabetes['Age'].isnull()
diabetes = diabetes.dropna()

diabetes = diabetes.drop_duplicates()

print('Number of data: ', len(diabetes))

# 1.3 Scatter plot of age and bmi
plt.figure()
plt.scatter(diabetes['Age'], diabetes['BMI'])
plt.xlabel('Age')
plt.ylabel('BMI')
plt.title('Age vs BMI')
plt.show()
# Vidimo kako je BMI u velikoj mjeri rasiren u dobi izmedu 20 i 40, dok nakon toga BMI ostaje unutar granica od 20 do 50

# 1.4 Min, Max and Mean of BMI
minBMI = diabetes['BMI'].min()
maxBMI = diabetes['BMI'].max()
meanBMI = diabetes['BMI'].mean()
print('Min BMI: ', minBMI, ' Max BMI: ', maxBMI, ' Mean BMI: ', meanBMI)

# 1.5 Same like 1.4 but for diabetes or no
diabetesTrue = diabetes[diabetes['Outcome'] == 1]
diabetesFalse = diabetes[diabetes['Outcome'] == 0]

diabetesTrueMinBMI = diabetesTrue['BMI'].min()
diabetesTrueMaxBMI = diabetesTrue['BMI'].max()
diabetesTrueMeanBMI = diabetesTrue['BMI'].mean()

diabetesFalseMinBMI = diabetesFalse['BMI'].min()
diabetesFalseMaxBMI = diabetesFalse['BMI'].max()
diabetesFalseMeanBMI = diabetesFalse['BMI'].mean()

print('Diabetes True Min BMI: ', diabetesTrueMinBMI, ' Max BMI: ', diabetesTrueMaxBMI, ' Mean BMI: ', diabetesTrueMeanBMI)
print('Diabetes False Min BMI: ', diabetesFalseMinBMI, ' Max BMI: ', diabetesFalseMaxBMI, ' Mean BMI: ', diabetesFalseMeanBMI)
